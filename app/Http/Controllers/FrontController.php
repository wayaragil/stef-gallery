<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FrontController extends Controller
{
    public function getIndex()
    {
        return view('front.index');
    }
    public function getRekamJejak()
    {
        $data['rekam_jejak'] = DB::table('rekam_jejak')
            ->orderBy('id', 'desc')
            ->paginate(15);

        return view('front.rekam_jejak', $data);
    }
    public function getKaryaPerwajahan()
    {
        $data['karya_perwajahan'] = DB::table('karya_perwajahan')
            ->orderBy('id', 'desc')
            ->paginate(15);

        return view('front.karya', $data);
    }
}
