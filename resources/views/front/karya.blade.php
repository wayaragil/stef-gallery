<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Karya Perwajahan</title>
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/lightbox2/css/lightbox.min.css') }}">
</head>
<body>

<div class="container mt-60">
    <h1 class="title">galery karya perwajahan terbaik lintas generasi</h1>

    <a href="{{ url('') }}" class="back mb-3">&lt; Back</a>

    <div class="row">
        @foreach($karya_perwajahan as $karya)
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="custom-img" onclick="showModal({{ $karya->id }})">
                <img src="{{ $karya->image }}" alt="{{ $karya->nama }}" class="lazy" data-src="{{ $karya->image }}">
            </div>

            <div class="modal fade karya-{{ $karya->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Made By {{ $karya->nama }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-img">
                            <div class="img-overlay" onclick="this.nextElementSibling.click()"></div>
                            <a href="{{ $karya->image }}" data-lightbox="{{ $karya->id }}" data-title="{{ $karya->nama }}">
                                <img src="{{ $karya->image }}" class="card-img-top" alt="{{ $karya->nama }}">
                            </a>
                        </div>
                        <table class="table">
                            <tr>
                                <th>Nama Siswa</th>
                                <td class="dot">:</td>
                                <td>{{ $karya->nama }}</td>
                            </tr>
                            <tr>
                                <th>Kelas</th>
                                <td class="dot">:</td>
                                <td>{{ $karya->kelas }}</td>
                            </tr>
                            <tr>
                                <th>Tahun</th>
                                <td class="dot">:</td>
                                <td>{{ $karya->tahun }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            </div>
        </div>
        @endforeach

        <div class="col-lg-12">
            {{-- @if(count($karya_perwajahan) != 0) --}}
            {{ $karya_perwajahan->links() }}
            {{-- @endif --}}
        </div>
    </div>

</div>

<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/lightbox2/js/lightbox.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.4.0/dist/lazyload.min.js"></script>
<script>
    function showModal(target) {
        let id = `.karya-${target}`;
        $(id).modal('show');
    }
    var lazyLoadImg = new LazyLoad({
        elements_selector: ".lazy",
        load_delay: 300
    });
</script>
</body>
</html>