<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Welcome to My Gallery</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <style>
        .main {
            text-align: center;
        }
    </style>
</head>
<body>

    <main role="main" class="main">
        <div class="front-background"></div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <h1 class="someone">Stefanus</h1>
                </div>
                <div class="col-lg-6 col-sm-12">
                    {{-- <div class="img-index" style="background-image: url('{{ asset('images/sit.jpg') }}')"></div> --}}
                    <img src="{{ asset('images/sit.jpg') }}" alt="Image" class="img-index">
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <nav class="mb-5">
                        <ul class="pagination pagination-lg justify-content-center">
                            <li class="page-item"><a class="page-link" href="{{ action('FrontController@getKaryaPerwajahan') }}">Gallery Karya Perwajahan</a></li>
                            <li class="page-item"><a class="page-link" href="{{ action('FrontController@getRekamJejak') }}">Gallery Rekam Jejak</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </main>

</body>
</html>