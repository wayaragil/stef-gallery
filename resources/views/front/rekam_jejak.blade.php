<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Rekam Jejak</title>
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/lightbox2/css/lightbox.min.css') }}">
</head>
<body>
<div class="container mt-60">
    <h1 class="title">Gallery Rekam Jejak</h1>

    <a href="{{ url('') }}" class="back mb-3">&lt; Back</a>

    <div class="row">
        @foreach($rekam_jejak as $rj)
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="custom-img" onclick="showModal('{{ $rj->id }}')">
                <img src="{{ $rj->image }}" alt="{{ $rj->nama }}" class="lazy" alt="Loading Image">
            </div>

            <div class="modal fade rekamjejak-{{ $rj->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Rekam Jejak Jarkoni++</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-img">
                            <div class="img-overlay" onclick="this.nextElementSibling.click()"></div>
                            <a href="{{ $rj->image }}" data-lightbox="{{ $rj->id }}" data-title="{{ $rj->nama }}">
                                <img src="{{ $rj->image }}" class="card-img-top" alt="{{ $rj->nama }}">
                            </a>
                        </div>
                        <table class="table">
                            <tr>
                                <th>rekam Jejak Jarkoni ++ </th>
                                <td class="dot">:</td>
                                <td>{{ $rj->rekam }}</td>
                            </tr>
                            <tr>
                                <th>Tahun</th>
                                <td class="dot">:</td>
                                <td>{{ $rj->tahun }}</td>
                            </tr>
                            <tr>
                                <th>Cerita dibalik cerita</th>
                                <td class="dot">:</td>
                                <td>{{ $rj->history }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            </div>
        </div>
        @endforeach
    </div>

</div>

<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/lightbox2/js/lightbox.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.4.0/dist/lazyload.min.js"></script>
<script>
    function showModal(target) {
        let id = `.rekamjejak-${target}`;
        $(id).modal('show');
    }
    var lazyLoadImg = new LazyLoad({
        elements_selector: ".lazy",
        load_delay: 300
    });
</script>
</body>
</html>